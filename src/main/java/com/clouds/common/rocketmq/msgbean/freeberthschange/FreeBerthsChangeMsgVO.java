package com.clouds.common.rocketmq.msgbean.freeberthschange;

import com.clouds.common.rocketmq.msgbean.base.BaseMQMessageVO;

/**
 * 空闲车位通知消息对象
 * .<br/>
 * 
 * Copyright: Copyright (c) 2017  zteits
 * 
 * @ClassName: FreeBerthsChangeVO
 * @Description: 
 * @version: v1.0.0
 * @author: zhaowg
 * @date: 2018年3月2日 下午1:33:17
 * Modification History:
 * Date             Author          Version            Description
 *---------------------------------------------------------*
 * 2018年3月2日      zhaowg           v1.0.0               创建
 */
public class FreeBerthsChangeMsgVO extends BaseMQMessageVO{
	/**停车场编号*/
	private String plNo;
	/**停车场名称*/
	private String plName;
	/**当前空闲车位数*/
	private int freeBerths;
	public FreeBerthsChangeMsgVO(String plNo, String plName, int freeBerths) {
		super();
		this.plNo = plNo;
		this.plName = plName;
		this.freeBerths = freeBerths;
	}
	public String getPlNo() {
		return plNo;
	}
	public void setPlNo(String plNo) {
		this.plNo = plNo;
	}
	public String getPlName() {
		return plName;
	}
	public void setPlName(String plName) {
		this.plName = plName;
	}
	public int getFreeBerths() {
		return freeBerths;
	}
	public void setFreeBerths(int freeBerths) {
		this.freeBerths = freeBerths;
	}
	@Override
	public String toString() {
		return "FreeBerthsChangeMsgVO [plNo=" + plNo + ", plName=" + plName + ", freeBerths=" + freeBerths + "]";
	}
	
}
